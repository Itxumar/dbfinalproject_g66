﻿namespace HSMS_Hostel_Manager.Behaviors;

public enum NavigationViewHeaderMode
{
    Always,
    Never,
    Minimal
}
