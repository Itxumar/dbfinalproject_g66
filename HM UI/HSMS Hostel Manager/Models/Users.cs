﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HSMS_Hostel_Manager.Models;

public class Users
{
    public string Id
    {
        get;
        set;
    }
    public string? FirstName
    {
        get;
        set;
    }
    public string? LastName
    {
        get;
        set;
    }
    public string? Username
    {
        get;
        set;
    }
    public string? Password
    {
        get;
        set;
    }
    public string? ContactNumber
    {
        get;
        set;
    }
    public string? Address
    {
        get;
        set;
    }
    public string? City
    {
        get;
        set;
    }
    public string DateofBirth
    {
        get;
        set;
    }
    public string? Email
    {
        get;
        set;
    }
    public string? Gender
    {
        get;
        set;
    }
    public Users(string Id, string FirstName, string LastName, string Username, string Password, string ContactNumber, string DateOfBirth, string Email, string Address, string City, string Gender)
    {
        this.Id = Id;
        this.FirstName = FirstName;
        this.LastName = LastName;
        this.ContactNumber = ContactNumber;
        this.Address = Address;
        this.City = City;
        this.DateofBirth = DateOfBirth;
        this.Email = Email;
        this.Gender = Gender;
        this.Username = Username;
        this.Password = Password;
    }
}
