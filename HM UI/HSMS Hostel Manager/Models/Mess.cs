﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.UI.Xaml;

namespace HSMS_Hostel_Manager.Models;

class Mess
{
    public string MessId
    {
    get; set; }
    public string ItemName
    {
    get; set; }
    public string Itemprice
    {
    get; set; }
    public string dayofweek
    {
    get; set; }

    public Mess(string MessId, string ItemName, string Itemprice, string dayofweek)
    {
        this.MessId = MessId;
        this.ItemName = ItemName;
        this.Itemprice = Itemprice;
        this.dayofweek = dayofweek;
    }


}
