﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.UI.Xaml.Controls.Primitives;

namespace HSMS_Hostel_Manager.Models;

public class Staff : Users
{
    public string StaffRole
    {
        get; set; 
    }
    public string HostelId
    {
        get; set;
    }
    public Staff(string Id, string FirstName, string LastName, string Username, string Password, string ContactNumber, string DateOfBirth, string Email, string Address, string City, string Gender, string StaffRole, string HostelId)
        : base(Id,FirstName,LastName,Username,Password,ContactNumber,DateOfBirth,Email,Address,City,Gender)
    {
        this.StaffRole = StaffRole;
        this.HostelId = HostelId;
    }
}
