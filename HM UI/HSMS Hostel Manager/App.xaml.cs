﻿using HSMS_Hostel_Manager.Activation;
using HSMS_Hostel_Manager.Contracts.Services;
using HSMS_Hostel_Manager.Core.Contracts.Services;
using HSMS_Hostel_Manager.Core.Services;
using HSMS_Hostel_Manager.Helpers;
using HSMS_Hostel_Manager.Models;
using HSMS_Hostel_Manager.Services;
using HSMS_Hostel_Manager.ViewModels;
using HSMS_Hostel_Manager.Views;

using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.UI.Xaml;

namespace HSMS_Hostel_Manager;

// To learn more about WinUI 3, see https://docs.microsoft.com/windows/apps/winui/winui3/.
public partial class App : Application
{
    // The .NET Generic Host provides dependency injection, configuration, logging, and other services.
    // https://docs.microsoft.com/dotnet/core/extensions/generic-host
    // https://docs.microsoft.com/dotnet/core/extensions/dependency-injection
    // https://docs.microsoft.com/dotnet/core/extensions/configuration
    // https://docs.microsoft.com/dotnet/core/extensions/logging
    public IHost Host
    {
        get;
    }

    public static T GetService<T>()
        where T : class
    {
        if ((App.Current as App)!.Host.Services.GetService(typeof(T)) is not T service)
        {
            throw new ArgumentException($"{typeof(T)} needs to be registered in ConfigureServices within App.xaml.cs.");
        }

        return service;
    }

    public static WindowEx MainWindow { get; } = new MainWindow();

    public App()
    {
        InitializeComponent();

        Host = Microsoft.Extensions.Hosting.Host.
        CreateDefaultBuilder().
        UseContentRoot(AppContext.BaseDirectory).
        ConfigureServices((context, services) =>
        {
            // Default Activation Handler
            services.AddTransient<ActivationHandler<LaunchActivatedEventArgs>, DefaultActivationHandler>();

            // Other Activation Handlers

            // Services
            services.AddSingleton<ILocalSettingsService, LocalSettingsService>();
            services.AddSingleton<IThemeSelectorService, ThemeSelectorService>();
            services.AddTransient<INavigationViewService, NavigationViewService>();

            services.AddSingleton<IActivationService, ActivationService>();
            services.AddSingleton<IPageService, PageService>();
            services.AddSingleton<INavigationService, NavigationService>();

            // Core Services
            services.AddSingleton<IFileService, FileService>();

            // Views and ViewModels
            services.AddTransient<AllotAHostelViewModel>();
            services.AddTransient<AllotAHostelPage>();
            services.AddTransient<UpdateStaffViewModel>();
            services.AddTransient<UpdateStaffPage>();
            services.AddTransient<ViewStaffDetailsViewModel>();
            services.AddTransient<ViewStaffDetailsPage>();
            services.AddTransient<AddStaffViewModel>();
            services.AddTransient<AddStaffPage>();
            services.AddTransient<UpdateMessScheduleViewModel>();
            services.AddTransient<UpdateMessSchedulePage>();
            services.AddTransient<ViewMessScheduleViewModel>();
            services.AddTransient<ViewMessSchedulePage>();
            services.AddTransient<AddMessScheduleViewModel>();
            services.AddTransient<AddMessSchedulePage>();
            services.AddTransient<MessManagementViewModel>();
            services.AddTransient<ViewHostelApplicationsViewModel>();
            services.AddTransient<ViewHostelApplicationsPage>();
            services.AddTransient<ViewPersonalInformationViewModel>();
            services.AddTransient<ViewPersonalInformationPage>();
            services.AddTransient<SettingsViewModel>();
            services.AddTransient<SettingsPage>();
            services.AddTransient<HomeViewModel>();
            services.AddTransient<HomePage>();
            services.AddTransient<ShellPage>();
            services.AddTransient<ShellViewModel>();

            // Configuration
            services.Configure<LocalSettingsOptions>(context.Configuration.GetSection(nameof(LocalSettingsOptions)));
        }).
        Build();

        UnhandledException += App_UnhandledException;
    }

    private void App_UnhandledException(object sender, Microsoft.UI.Xaml.UnhandledExceptionEventArgs e)
    {
        // TODO: Log and handle exceptions as appropriate.
        // https://docs.microsoft.com/windows/windows-app-sdk/api/winrt/microsoft.ui.xaml.application.unhandledexception.
    }

    protected async override void OnLaunched(LaunchActivatedEventArgs args)
    {
        base.OnLaunched(args);

        await App.GetService<IActivationService>().ActivateAsync(args);
    }
}
