﻿namespace HSMS_Hostel_Manager.Contracts.Services;

public interface IPageService
{
    Type GetPageType(string key);
}
