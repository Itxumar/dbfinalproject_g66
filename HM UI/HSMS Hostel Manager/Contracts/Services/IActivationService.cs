﻿namespace HSMS_Hostel_Manager.Contracts.Services;

public interface IActivationService
{
    Task ActivateAsync(object activationArgs);
}
