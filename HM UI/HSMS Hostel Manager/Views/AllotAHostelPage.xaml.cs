﻿using HSMS_Hostel_Manager.ViewModels;

using Microsoft.UI.Xaml.Controls;

namespace HSMS_Hostel_Manager.Views;

public sealed partial class AllotAHostelPage : Page
{
    public AllotAHostelViewModel ViewModel
    {
        get;
    }

    public AllotAHostelPage()
    {
        ViewModel = App.GetService<AllotAHostelViewModel>();
        InitializeComponent();
    }
}
