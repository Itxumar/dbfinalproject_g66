﻿using HSMS_Hostel_Manager.ViewModels;
using Microsoft.UI.Xaml;
using Microsoft.UI.Xaml.Controls;

namespace HSMS_Hostel_Manager.Views;

public sealed partial class UpdateStaffPage : Page
{
    public UpdateStaffViewModel ViewModel
    {
        get;
    }

    public UpdateStaffPage()
    {
        ViewModel = App.GetService<UpdateStaffViewModel>();
        InitializeComponent();
    }
    public void SearchServant_ButtonClicked(object sender, RoutedEventArgs e)
    {
        
    }

    public void UpdateServant_ButtonClicked(object sender, RoutedEventArgs e)
    {
    
    }
}
