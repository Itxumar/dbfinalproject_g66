﻿using HSMS_Hostel_Manager.ViewModels;

using Microsoft.UI.Xaml.Controls;

namespace HSMS_Hostel_Manager.Views;

public sealed partial class UpdateMessSchedulePage : Page
{
    public UpdateMessScheduleViewModel ViewModel
    {
        get;
    }

    public UpdateMessSchedulePage()
    {
        ViewModel = App.GetService<UpdateMessScheduleViewModel>();
        InitializeComponent();
    }
}
