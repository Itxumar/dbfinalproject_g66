﻿using System.Data.SqlClient;
using System.Data;
using Microsoft.UI.Xaml.Controls;
using Microsoft.UI.Xaml;
using CommunityToolkit.WinUI.UI.Controls;
using Microsoft.UI.Xaml.Data;
using CommunityToolkit.Common.Collections;
using HSMS_Hostel_Manager.Models;

namespace HSMS_Hostel_Manager.Views;

#pragma warning disable CS8602
#pragma warning disable CS8603
#pragma warning disable CS8604
#pragma warning disable IDE0019
#pragma warning disable CS0659
#pragma warning disable CS8765


public class Helper
{
    public static void FillComboBox(XamlRoot xamlRoot, string query, ComboBox comboBox)
    {
        var con = Configuration.getInstance().getConnection();

        var cmd = new SqlCommand(query, con);

        try
        {
            var adapter = new SqlDataAdapter(cmd);

            var dataTable = new DataTable();

            adapter.Fill(dataTable);

            foreach (DataRow row in dataTable.Rows)
            {
                try
                {
                    comboBox.Items.Add(row["Value"]);

                }
                catch (Exception error)
                {
                    ShowMessageBox(xamlRoot, "Error", error.Message);
                    return;
                }
            }
        }
        catch (Exception error)
        {
            ShowMessageBox(xamlRoot, "Error", error.Message);
            return;
        }
    }

    public static async void ShowMessageBox(XamlRoot xamlRoot, string title, string content)
    {
        var dialog = new ContentDialog()
        {
            XamlRoot = xamlRoot,
            Title = title,
            Content = content,
            PrimaryButtonText = "Ok",
            DefaultButton = ContentDialogButton.Primary
        };

        await dialog.ShowAsync();
    }

    public static async void ShowErrorMessage(XamlRoot xamlRoot, string message)
    {
        ContentDialog dialog = new ContentDialog();

        dialog.XamlRoot = xamlRoot;
        dialog.Style = Application.Current.Resources["DefaultContentDialogStyle"] as Style;
        dialog.Title = "Error";
        dialog.PrimaryButtonText = "Discard";
        dialog.DefaultButton = ContentDialogButton.Primary;
        dialog.Content = message;

        await dialog.ShowAsync();
        return;
    }

    public static async Task<int> ShowConfirmationDialog(XamlRoot xamlRoot, string message)
    {
        ContentDialog dialog = new ContentDialog();

        dialog.XamlRoot = xamlRoot;
        dialog.Style = Application.Current.Resources["DefaultContentDialogStyle"] as Style;
        dialog.Title = "Confirmation";
        dialog.PrimaryButtonText = "Yes";
        dialog.SecondaryButtonText = "No";
        dialog.DefaultButton = ContentDialogButton.Primary;
        dialog.Content = message;

        var result = await dialog.ShowAsync();

        if (result == ContentDialogResult.Primary)
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }


    public static void FillDataGrid(string query, DataGrid dataGrid, string TableName)
    {
        var con = Configuration.getInstance().getConnection();

        SqlCommand command = new SqlCommand(query, con);

        SqlDataAdapter adapter = new SqlDataAdapter(command);

        DataTable dataTable = new DataTable();

        adapter.Fill(dataTable);

        if (TableName == "Staff")
        {
            var itemsArray = new Staff[dataTable.Rows.Count];

            for (var i = 0; i < dataTable.Rows.Count; i++)
            {
                var Id = dataTable.Rows[i][0].ToString();
                var FirstName = dataTable.Rows[i][1].ToString();
                var LastName = dataTable.Rows[i][2].ToString();
                var Username = dataTable.Rows[i][3].ToString();
                var Password = dataTable.Rows[i][4].ToString();
                var Contact = dataTable.Rows[i][5].ToString();
                var Email = dataTable.Rows[i][6].ToString();
                var DateOfBirth = dataTable.Rows[i][7].ToString();
                var City = dataTable.Rows[i][8].ToString();
                var Address = dataTable.Rows[i][9].ToString();
                var Gender = dataTable.Rows[i][10].ToString();
                var StaffRole = dataTable.Rows[i][11].ToString();
                var hostelID = dataTable.Rows[i][12].ToString();
                var x = new Staff(Id, FirstName, LastName, Username, Password, Contact, Email, DateOfBirth, City, Address, Gender,StaffRole,hostelID);
                itemsArray[i] = x;
            }

            dataGrid.ItemsSource = itemsArray;
        }
        else if (TableName == "Mess")
        {
            var itemsArray = new Mess[dataTable.Rows.Count];

            for (var i = 0; i < dataTable.Rows.Count; i++)
            {
                var messId = dataTable.Rows[i][0].ToString();
                var ItemName = dataTable.Rows[i][1].ToString();
                var Itemprice = dataTable.Rows[i][2].ToString();
                var DayofWeek = dataTable.Rows[i][3].ToString();
                
                var x = new Mess(messId,ItemName,Itemprice,DayofWeek);
                itemsArray[i] = x;
            }

            dataGrid.ItemsSource = itemsArray;



        }

        
    }

    /*public static Staff? GetStafftById(XamlRoot xamlRoot, string Id)
    {
        var con = Configuration.getInstance().getConnection();
        var query = "SELECT s.Id AS [Id], p.FirstName AS [First Name], COALESCE(p.LastName, 'NULL') AS [Last Name], COALESCE(p.Contact, 'NULL') AS [Contact], p.Email, COALESCE(CONVERT(varchar(10), p.DateOfBirth, 105), 'NULL') AS [DateOfBirth], COALESCE(l.Value, 'NULL') AS [Gender] FROM Student s JOIN Person p ON s.Id = p.Id JOIN Lookup l ON p.Gender = l.Id AND l.Category = 'Gender' WHERE s.Id = @Id";

        var cmd = new SqlCommand(query, con);
        cmd.Parameters.AddWithValue("@Id", Id);

        try
        {
            var adapter = new SqlDataAdapter(cmd);

            var dataTable = new DataTable();

            adapter.Fill(dataTable);

            if (dataTable.Rows.Count <= 0)
            {
                return null;
            }

            var dataRow = dataTable.Rows[0];

            var Id_New = dataRow["Id"].ToString();
            var FirstName = dataRow["First Name"].ToString();
            var LastName = dataRow["Last Name"].ToString();
            var Username = dataRow["Username"].ToString();
            var Password = dataRow["Password"].ToString();
            var Contact = dataRow["ContactNumber"].ToString();
            var Email = dataRow["Email"].ToString();
            var Address = dataRow["Address"].ToString();
            var DateOfBirth = dataRow["DateOfBirth"].ToString();
            var City = dataRow["City"].ToString();
            var Gender = dataRow["Gender"].ToString();
            var StaffRole = dataRow["StaffRole"].ToString();
            var hostelid = int.Parse((string)dataRow["HostelID"]);

            var s = new Staff(Id_New, FirstName, LastName, Username, Password, Contact, Email, Address, DateOfBirth, City, Gender,StaffRole,hostelid);

            return s;

        }
        catch (Exception exp)
        {
            Helper.ShowErrorMessage(xamlRoot, exp.Message);
            return null;
        }
    }
*/
    public static void ExecuteQuery(XamlRoot xamlRoot, string query)
    {
        var connection = Configuration.getInstance().getConnection();

        var command = new SqlCommand(query, connection);

        try
        {
            command.ExecuteNonQuery();
        }
        catch (Exception error)
        {
            ShowErrorMessage(xamlRoot, error.Message);
            return;
        }
    }

    public static void FillColumns(string query, DataGrid dataGrid)
    {
        var con = Configuration.getInstance().getConnection();

        SqlCommand command = new SqlCommand(query, con);

        SqlDataAdapter adapter = new SqlDataAdapter(command);

        DataTable dataTable = new DataTable();

        adapter.Fill(dataTable);

        dataGrid.Columns.Clear();
        dataGrid.ItemsSource = null;
        dataGrid.AutoGenerateColumns = false;

        for (var i = 0; i < dataTable.Columns.Count; i++)
        {
             if (dataTable.Columns[i].ColumnName == "Project_Description")
             {
                 dataGrid.Columns.Add(new DataGridTextColumn()
                 {
                     Header = dataTable.Columns[i].ColumnName,
                     Binding = new Binding { Path = new PropertyPath(dataTable.Columns[i].ColumnName) },
                     Width = new DataGridLength(200),
                     ElementStyle = new Style(typeof(TextBlock))
                     {
                         Setters =
                             {
                                 new Setter(TextBlock.TextWrappingProperty, TextWrapping.Wrap)
                             }
                     }
                 });

                 continue;
             }

            dataGrid.Columns.Add(new DataGridTextColumn()
            {
                Header = dataTable.Columns[i].ColumnName,
                Binding = new Binding { Path = new PropertyPath(dataTable.Columns[i].ColumnName) }
            });
        }
    }
}

