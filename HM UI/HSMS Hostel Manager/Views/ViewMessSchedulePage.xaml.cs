﻿using HSMS_Hostel_Manager.ViewModels;

using Microsoft.UI.Xaml.Controls;

namespace HSMS_Hostel_Manager.Views;

public sealed partial class ViewMessSchedulePage : Page
{
    public ViewMessScheduleViewModel ViewModel
    {
        get;
    }

    public ViewMessSchedulePage()
    {
        ViewModel = App.GetService<ViewMessScheduleViewModel>();
        InitializeComponent();
    }
}
