﻿using HSMS_Hostel_Manager.ViewModels;

using Microsoft.UI.Xaml.Controls;

namespace HSMS_Hostel_Manager.Views;

public sealed partial class ViewPersonalInformationPage : Page
{
    public ViewPersonalInformationViewModel ViewModel
    {
        get;
    }

    public ViewPersonalInformationPage()
    {
        ViewModel = App.GetService<ViewPersonalInformationViewModel>();
        InitializeComponent();
    }
}
