﻿using HSMS_Hostel_Manager.ViewModels;

using Microsoft.UI.Xaml.Controls;

namespace HSMS_Hostel_Manager.Views;

public sealed partial class ViewHostelApplicationsPage : Page
{
    public ViewHostelApplicationsViewModel ViewModel
    {
        get;
    }

    public ViewHostelApplicationsPage()
    {
        ViewModel = App.GetService<ViewHostelApplicationsViewModel>();
        InitializeComponent();
    }
}
