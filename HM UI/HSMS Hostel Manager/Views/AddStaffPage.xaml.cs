﻿using HSMS_Hostel_Manager.ViewModels;
using Microsoft.UI.Xaml;
using Microsoft.UI.Xaml.Controls;
using HSMS_Hostel_Manager.Models;
using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;


namespace HSMS_Hostel_Manager.Views;

public sealed partial class AddStaffPage : Page
{
    public AddStaffViewModel ViewModel
    {
        get;
    }

    public AddStaffPage()
    {
        ViewModel = App.GetService<AddStaffViewModel>();
        InitializeComponent();
        var query = "Select Value from lookup Where Category = 'Gender'";
        var query1 = "Select Value from Lookup Where Category = 'STAFF ROLE'";
        var query3 = "Select HostelID from Hostel";

        Helper.FillComboBox(XamlRoot, query, Gender_ComboBox);
        Helper.FillComboBox(XamlRoot, query1, Role_ComboBox);
        Helper.FillComboBox(XamlRoot, query3, ID_ComboBox);

    }

    public void AddServant_ButtonClicked(object sender, RoutedEventArgs e)
    {
        var firstname = FirstName_TextBox.Text;
        var lastname = LastName_TextBox.Text;
        var email = Email_TextBox.Text;
        var username = Username_TextBox.Text;
        var password = Password_TextBox.Text;
        var contactnumber = ContactNumber_TextBox.Text;
        var address = Address_TextBox.Text;
        var city = City_TextBox.Text;
        var gender = Gender_ComboBox.SelectedIndex + 1;
        var role = Role_ComboBox.SelectedIndex + 30;
        var dateofbirth = DateofBirth_DatePicker.Date;
        var hostelid = ID_ComboBox.SelectedIndex;

        var con = Configuration.getInstance().getConnection();
        SqlCommand cmd = new SqlCommand("Insert Into Users (Username,Password,FirstName,LastName,ContactNumber,Email,Address,DateOfBirth,City,Gender) values (@Username,@Password,@FirstName,@LastName,@ContactNumber,@Email,@Address,@DateOfBirth,@City,@Gender)", con);
        cmd.Parameters.AddWithValue("@Username", username);
        cmd.Parameters.AddWithValue("@Password", password);
        cmd.Parameters.AddWithValue("@FirstName", firstname);
        cmd.Parameters.AddWithValue("@LastName", lastname);
        cmd.Parameters.AddWithValue("@ContactNumber", contactnumber);
        cmd.Parameters.AddWithValue("@Email", email);
        cmd.Parameters.AddWithValue("@Address", address);
        cmd.Parameters.AddWithValue("@DateOfBirth", dateofbirth);
        cmd.Parameters.AddWithValue("@City", city);

        cmd.Parameters.AddWithValue("@Gender", gender);

        cmd.ExecuteNonQuery();

        SqlCommand newcmd = new SqlCommand("Insert Into Staff(HostelID,StaffRole) Values(@HostelID,@StaffRole)", con);
        newcmd.Parameters.AddWithValue("@HostelID",hostelid);
        newcmd.Parameters.AddWithValue("@StaffRole", role);

        newcmd.ExecuteNonQuery();

        Helper.ShowMessageBox(XamlRoot,"Success","Successfully saved");

    }
}
