﻿using System.Data.SqlClient;

using HSMS_Hostel_Manager.Models;
using HSMS_Hostel_Manager.ViewModels;
using Microsoft.UI.Xaml;
using Microsoft.UI.Xaml.Controls;

namespace HSMS_Hostel_Manager.Views;

public sealed partial class AddMessSchedulePage : Page
{
    public AddMessScheduleViewModel ViewModel
    {
        get;
    }

    public AddMessSchedulePage()
    {
        ViewModel = App.GetService<AddMessScheduleViewModel>();
        InitializeComponent();

        var query = "Select Value From Lookup Where Category = 'WEEK DAY'";
        Helper.FillComboBox(XamlRoot, query, Week_ComboBox);
        
    }
    public void AddMess_ButtonClicked(object sender, RoutedEventArgs e)
    {
        var MessId = Id_TextBox.Text;
        var itemname = Name_TextBox.Text;
        var itemprice = Price_TextBox.Text;
        var dayofweek = Week_ComboBox.SelectedIndex + 34;

        var con = Configuration.getInstance().getConnection();
        SqlCommand cmd = new SqlCommand("Insert Into MessFood (MessId,ItemName,ItemPrice,DayofWeek) values (@MessId,@ItemName,@ItemPrice,@DayofWeek)", con);
        cmd.Parameters.AddWithValue("@MessId", MessId);
        cmd.Parameters.AddWithValue("@ItemName", itemname);
        cmd.Parameters.AddWithValue("@ItemPrice", itemprice);
        cmd.Parameters.AddWithValue("@DayofWeek", dayofweek);

        cmd.ExecuteNonQuery();
        Helper.ShowMessageBox(XamlRoot, "Success", "Data Added Successfully");
    }
}
