﻿using HSMS_Hostel_Manager.ViewModels;

using Microsoft.UI.Xaml.Controls;

namespace HSMS_Hostel_Manager.Views;

public sealed partial class ViewStaffDetailsPage : Page
{
    public ViewStaffDetailsViewModel ViewModel
    {
        get;
    }

    public ViewStaffDetailsPage()
    {
        ViewModel = App.GetService<ViewStaffDetailsViewModel>();
        InitializeComponent();
    }
}
