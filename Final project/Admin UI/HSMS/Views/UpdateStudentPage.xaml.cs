﻿using HSMS.ViewModels;
using System.Diagnostics;
using Microsoft.UI.Xaml;
using HSMS.Models;
using Microsoft.UI.Xaml.Controls;
using Microsoft.UI.Xaml.Navigation;
using CommunityToolkit.WinUI.UI.Controls;
using Windows.System;

namespace HSMS.Views;

public sealed partial class UpdateStudentPage : Page
{
    public UpdateStudentViewModel ViewModel
    {
        get;
    }

    public UpdateStudentPage()
    {
        ViewModel = App.GetService<UpdateStudentViewModel>();
        InitializeComponent();
    }

    private void Search_ButtonClicked(object Sender, RoutedEventArgs e)
    {
        var Id = UpdateStudentID_TextBox.Text;
        var query = "Select u.Id,u.FirstName,u.LastName,u.Username,u.Password,u.ContactNumber,u.DateOfBirth,u.Email,u.Address,u.City,(Select Value From Lookup Where Id = u.Gender) AS Gender, h.HostelId From Users u JOIN HostelManager h ON u.Id = h.ManagerId Where u.";
        Helper.GetStudentById(XamlRoot, query);
    }

    private void UpdateStudent_ButtonClicked(object sender, RoutedEventArgs e)
    {

    }


    // On Update Page -- Add this function
    protected override void OnNavigatedTo(NavigationEventArgs e)
    {

        base.OnNavigatedTo(e);

        //Get the Object passed from View Grid Page and store in variable
         var s = (Student)e.Parameter;


        // Validation
        if (s is null)
        {
             return;
        }
        // fill all textboxes according to values in object
        FillValues(s);

        // Unique attributes are not allowed to change so make those textboxes readonly
        // StudentId_TextBox.IsReadOnly = true;

        // Hide the search button
        Search_Button.Visibility = Visibility.Collapsed;
    }

    private void FillValues(Users user)
    {
        UpdateStudentID_TextBox.Text = user.Id.ToString();
        UpdateFirstName_TextBox.Text = user.FirstName.ToString();
        UpdateLastName_TextBox.Text = user.LastName.ToString();
        if (user.Username != null)
        {
            UpdateUsername_TextBox.Text = user.Username.ToString();
        }
        if (user.Password != null)
        {
            UpdatePassword_TextBox.Text = user.Password.ToString();
        }
        UpdateContactNumber_TextBox.Text = user.ContactNumber.ToString();
        UpdateEmail_TextBox.Text = user.Email.ToString();
        UpdateAddress_TextBox.Text = user.Address.ToString();
        DateOfBirth_DatePicker.SelectedDate = DateTimeOffset.Parse(user.DateofBirth);
        UpdateCity_TextBox.Text = user.City.ToString();
        UpdateGender_ComboBox.SelectedIndex = user.Gender == "Male" ? 0 : 1;
    }
}

