﻿using System.Data.SqlClient;
using System.Diagnostics;
using HSMS.ViewModels;
using Microsoft.UI.Xaml;
using Microsoft.UI.Xaml.Controls;

namespace HSMS.Views;

public sealed partial class AddStudentPage : Page
{
    public AddStudentViewModel ViewModel
    {
        get;
    }

    public AddStudentPage()
    {
        ViewModel = App.GetService<AddStudentViewModel>();
        InitializeComponent();
        var query = "Select Value from Lookup Where Category = 'Gender'";
        Helper.FillComboBox(XamlRoot, query, Gender_ComboBox);
        query = "Select Value from Lookup Where Category = 'Department_Name'";
        Helper.FillComboBox(XamlRoot, query, Department_ComboBox);
    }


    private void AddStudent_ButtonClicked(object Sender, RoutedEventArgs e)
    {
        var firstname = FirstName_TextBox.Text;
        var lastname = LastName_TextBox.Text;
        var email = Email_TextBox.Text;
        var username = Username_TextBox.Text;
        var password = Password_TextBox.Text;
        var contactnumber = ContactNumber_TextBox.Text;
        var address = Address_TextBox.Text;
        var city = City_TextBox.Text;
        var ecatmarks = EcatMarks_TextBox.Text;
        var fscmarks = FscMarks_TextBox.Text;
        var matricmarks = MatricMarks_TextBox.Text;
        var registrationnumber = RegistrationNumber_TextBox.Text;
        var Dateofbirth = DateofBirth_DatePicker.Date;
        var gender = Gender_ComboBox.SelectedIndex + 1;
        var department = Department_ComboBox.SelectedIndex + 5;

         
        
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Insert Into Users (Username,Password,FirstName,LastName,ContactNumber,Email,Address,DateOfBirth,City,Gender) values (@Username,@Password,@FirstName,@LastName,@ContactNumber,@Email,@Address,@DateOfBirth,@City,@Gender)", con);
            cmd.Parameters.AddWithValue("@Username", username);
            cmd.Parameters.AddWithValue("@Password", password);
            cmd.Parameters.AddWithValue("@FirstName", firstname);
            cmd.Parameters.AddWithValue("@LastName", lastname);
            cmd.Parameters.AddWithValue("@ContactNumber", contactnumber);
            cmd.Parameters.AddWithValue("@Email", email);
            cmd.Parameters.AddWithValue("@Address", address);
            cmd.Parameters.AddWithValue("@DateOfBirth", Dateofbirth);
            cmd.Parameters.AddWithValue("@City", city);
  
            cmd.Parameters.AddWithValue("@Gender", gender); 

            cmd.ExecuteNonQuery();
            //Helper.ShowMessageBox(XamlRoot,"Success","Successfully saved");

            SqlCommand newcmd = new SqlCommand("Insert Into Student values ((Select MAX(Id) From Users),@RegistrationNumber,@EcatMarks,@FscMarks,@MatricMarks,@DepartmentName)", con);
            newcmd.Parameters.AddWithValue("@RegistrationNumber", registrationnumber);
            newcmd.Parameters.AddWithValue("@EcatMarks", ecatmarks);
            newcmd.Parameters.AddWithValue("@FscMarks", fscmarks);
            newcmd.Parameters.AddWithValue("@MatricMarks", matricmarks);
            newcmd.Parameters.AddWithValue("@DepartmentName",department);
            
            newcmd.ExecuteNonQuery(); 
            //Helper.ShowMessageBox(XamlRoot,"Success","Successfully saved");

    }
}
