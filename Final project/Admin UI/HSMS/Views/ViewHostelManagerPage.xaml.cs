﻿using HSMS.ViewModels;

using Microsoft.UI.Xaml.Controls;

namespace HSMS.Views;

public sealed partial class ViewHostelManagerPage : Page
{
    public ViewHostelManagerViewModel ViewModel
    {
        get;
    }

    public ViewHostelManagerPage()
    {
        ViewModel = App.GetService<ViewHostelManagerViewModel>();
        InitializeComponent();

        var query = "Select u.Id,u.FirstName,u.LastName,u.Username,u.Password,u.ContactNumber,u.DateOfBirth,u.Email,u.Address,u.City,(Select Value From Lookup Where Id = u.Gender) AS Gender, h.HostelId From Users u JOIN HostelManager h ON u.Id = h.ManagerId";
        Helper.FillColumns(query, dataGrid);

        Helper.FillDataGrid(query, dataGrid, "Hostel Manager");
    }
}
