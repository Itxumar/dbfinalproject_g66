﻿using HSMS.ViewModels;

using Microsoft.UI.Xaml.Controls;

namespace HSMS.Views;

public sealed partial class ViewComplaintsPage : Page
{
    public ViewComplaintsViewModel ViewModel
    {
        get;
    }

    public ViewComplaintsPage()
    {
        ViewModel = App.GetService<ViewComplaintsViewModel>();
        InitializeComponent();
    }
}
