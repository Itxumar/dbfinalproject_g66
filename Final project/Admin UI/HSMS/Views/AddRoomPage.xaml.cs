﻿using System.Data.SqlClient;
using HSMS.ViewModels;
using Microsoft.UI.Xaml;
using Microsoft.UI.Xaml.Controls;
using Windows.Networking;

namespace HSMS.Views;

public sealed partial class AddRoomPage : Page
{
    public AddRoomViewModel ViewModel
    {
        get;
    }

    public AddRoomPage()
    {
        ViewModel = App.GetService<AddRoomViewModel>();
        InitializeComponent();
        var Query1 = "Select Value From Lookup Where Category = 'ROOM_STATUS'";
        var Query = "Select HostelID From Hostel"; 

        Helper.FillComboBox(XamlRoot, Query, Id_ComboBox);

        Helper.FillComboBox(XamlRoot, Query1, RoomStatus_ComboBox);
    }

    public void AddRoom_ButtonClicked(object sender, RoutedEventArgs e)
    {
        var Hostelid = Id_ComboBox.SelectedIndex;
        var TotalNumberofrooms = RoomNo_TextBox.Text;
        var RoomStatus = RoomStatus_ComboBox.SelectedIndex + 3;

        var con = Configuration.getInstance().getConnection();
        SqlCommand cmd1 = new SqlCommand("Insert Into Room(HostelId,RoomNo,RoomStatus) values (@HostelId,@RoomNo,@RoomStatus)", con);
        cmd1.Parameters.AddWithValue("@HostelId", Hostelid);
        cmd1.Parameters.AddWithValue("@RoomNo", TotalNumberofrooms);
        cmd1.Parameters.AddWithValue("@RoomStatus", RoomStatus);

        cmd1.ExecuteNonQuery();

    }
}
