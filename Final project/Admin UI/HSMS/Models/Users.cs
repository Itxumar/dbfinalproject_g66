﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Windows.Networking;

namespace HSMS.Models;

public class Users
{
    public string Id
    {
        get;
        set;
    }
    public string? FirstName
    {
        get;
        set;
    }
    public string? LastName
    {
        get;
        set;
    }
    public string? Username
    {
        get;
        set;
    }
    public string? Password
    {
        get;
        set;
    }
    public string? ContactNumber
    {
        get;
        set;
    }
    public string? Address
    {
        get;
        set;
    }
    public string? City
    {
        get;
        set;
    }
    public string DateofBirth
    {
        get;
        set;
    }
    public string? Email
    {
        get;
        set;
    }
    public string? Gender
    {
        get;
        set;
    }

    /*public Users(string? firstName, string? lastName, string? contactNumber, string? address, string? city, string? dateofBirth, string? email, string? gender, string? username, string? password)
    {
        FirstName = firstName;
        LastName = lastName;
        ContactNumber = contactNumber;
        Address = address;
        City = city;
        DateofBirth = dateofBirth;
        Email = email;
        Gender = gender;
        Username = username;
        Password = password;
    }*/
    public Users(string Id, string FirstName, string LastName, string Username, string Password, string ContactNumber, string DateOfBirth, string Email, string Address, string City, string Gender)
    {
        this.Id = Id;
        this.FirstName = FirstName;
        this.LastName = LastName;
        this.ContactNumber = ContactNumber;
        this.Address = Address;
        this.City = City;
        this.DateofBirth = DateOfBirth;
        this.Email = Email;
        this.Gender = Gender;
        this.Username = Username;
        this.Password = Password;
    }
}
