﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HSMS.Models;
internal class Hostel
{
    public string Hostelname
    {
        get; set; 
    }
    public string Hosteltype
    {
        get; set; 
    }
    public Hostel(string hostelname, string hosteltype)
    {
        this.Hostelname = hostelname;
        this.Hosteltype = hosteltype;
    }
}
