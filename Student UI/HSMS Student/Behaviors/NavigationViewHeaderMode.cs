﻿namespace HSMS_Student.Behaviors;

public enum NavigationViewHeaderMode
{
    Always,
    Never,
    Minimal
}
