﻿namespace HSMS_Student.Contracts.Services;

public interface IActivationService
{
    Task ActivateAsync(object activationArgs);
}
