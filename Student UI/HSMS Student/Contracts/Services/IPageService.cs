﻿namespace HSMS_Student.Contracts.Services;

public interface IPageService
{
    Type GetPageType(string key);
}
