﻿using HSMS_Student.ViewModels;
using Microsoft.UI.Xaml;
using Microsoft.UI.Xaml.Controls;
using System.Diagnostics;
namespace HSMS_Student.Views;

public sealed partial class ApplyForHostelPage : Page
{
    public ApplyForHostelViewModel ViewModel
    {
        get;
    }

    public ApplyForHostelPage()
    {
        ViewModel = App.GetService<ApplyForHostelViewModel>();
        InitializeComponent();
    }

    private void Apply_ButtonClicked(object sender, RoutedEventArgs e)
    {
    
    }
}
