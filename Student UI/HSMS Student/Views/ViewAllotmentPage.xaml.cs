﻿using HSMS_Student.ViewModels;

using Microsoft.UI.Xaml.Controls;

namespace HSMS_Student.Views;

public sealed partial class ViewAllotmentPage : Page
{
    public ViewAllotmentViewModel ViewModel
    {
        get;
    }

    public ViewAllotmentPage()
    {
        ViewModel = App.GetService<ViewAllotmentViewModel>();
        InitializeComponent();
    }
}
