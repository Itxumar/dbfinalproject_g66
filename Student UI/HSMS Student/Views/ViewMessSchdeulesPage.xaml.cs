﻿using HSMS_Student.ViewModels;

using Microsoft.UI.Xaml.Controls;

namespace HSMS_Student.Views;

public sealed partial class ViewMessSchdeulesPage : Page
{
    public ViewMessSchdeulesViewModel ViewModel
    {
        get;
    }

    public ViewMessSchdeulesPage()
    {
        ViewModel = App.GetService<ViewMessSchdeulesViewModel>();
        InitializeComponent();
    }
}
