﻿using HSMS_Student.ViewModels;

using Microsoft.UI.Xaml.Controls;

namespace HSMS_Student.Views;

public sealed partial class ManageComplaintsPage : Page
{
    public ManageComplaintsViewModel ViewModel
    {
        get;
    }

    public ManageComplaintsPage()
    {
        ViewModel = App.GetService<ManageComplaintsViewModel>();
        InitializeComponent();
    }
}
