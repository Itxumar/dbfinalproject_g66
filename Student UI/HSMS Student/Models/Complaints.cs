﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HSMS_Student.Models;

class Complaints
{
    public string ComplaintType
    {
    get; set; }
    public string ComplaintDesc
    {
    get; set;}
    public string DateofFiling
    {
    get; set;}

    public string studentid
    {
    get; set;}

    public Complaints(string complaintType, string complaintDesc, string dateofFiling, string studentid)
    {
        ComplaintType = complaintType;
        ComplaintDesc = complaintDesc;
        DateofFiling = dateofFiling;
        this.studentid = studentid;
    }
}
