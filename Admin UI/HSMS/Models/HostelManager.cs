﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HSMS.Models;

class HostelManager : Users
{
    public string? ManagerID
    {
        get; set; 
    }
    public string HostelId
    {
        get; set; 
    }

    public HostelManager(string Id, string FirstName, string LastName, string Username, string Password, string ContactNumber, string DateOfBirth, string Email, string Address, string City, string Gender, string HostelId)
        : base(Id, FirstName, LastName, Username, Password, ContactNumber, DateOfBirth, Email, Address, City, Gender) {
        this.HostelId = HostelId;
    }

}
