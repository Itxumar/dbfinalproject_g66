﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommunityToolkit.WinUI.UI.Converters;

namespace HSMS.Models;
internal class Room
{
    public string HostelId
    {
    get; set; }
    public string Numberofrooms
    {
    get; set; }
    public string RoomStatus
    {
    get; set; }

    public Room(string hostelId, string numberofrooms, string roomStatus)
    {
        HostelId = hostelId;
        Numberofrooms = numberofrooms;
        RoomStatus = roomStatus;
    }
}
