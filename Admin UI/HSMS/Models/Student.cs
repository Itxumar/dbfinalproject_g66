﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Windows.Networking;

namespace HSMS.Models;

public class Student : Users
{
    public string? StudentId
    {
        get;
        set;
    }
    public string RegistrationNumber
    {
        get;
        set;
    }
    public string EcatMarks
    {
        get; 
        set; 
    }
    public string FscMarks
    {
        get; 
        set;
    }
    public string MatricMarks
    {
        get;
        set;
    }
    public string DepartmentName
    {
        get;
        set;
    }
    /*
        public Student(string StudentId, string registrationNumber, string ecatMarks, string fscMarks, string matricMarks, string departmentName)
        {
            this.StudentId = StudentId;
            this.RegistrationNumber = registrationNumber;
            EcatMarks = ecatMarks;
            FscMarks = fscMarks;
            MatricMarks = matricMarks;
            DepartmentName = departmentName;
        }*/

    public Student(string Id, string FirstName, string LastName, string Username, string Password, string ContactNumber, string DateOfBirth, string Email, string Address, string City, string Gender, string registrationNumber, string ecatMarks, string fscMarks, string matricMarks, string departmentName)
        :base(Id, FirstName, LastName, Username, Password, ContactNumber, DateOfBirth, Email, Address, City, Gender)
    {
        RegistrationNumber = registrationNumber;
        EcatMarks = ecatMarks;
        FscMarks = fscMarks;
        MatricMarks = matricMarks;
        DepartmentName = departmentName;
    }
}
