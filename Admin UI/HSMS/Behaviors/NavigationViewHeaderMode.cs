﻿namespace HSMS.Behaviors;

public enum NavigationViewHeaderMode
{
    Always,
    Never,
    Minimal
}
