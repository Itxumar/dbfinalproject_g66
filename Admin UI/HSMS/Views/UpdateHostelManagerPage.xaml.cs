﻿using System.Diagnostics;
using HSMS.Models;
using HSMS.ViewModels;
using Microsoft.UI.Xaml;

using Microsoft.UI.Xaml.Controls;

namespace HSMS.Views;

public sealed partial class UpdateHostelManagerPage : Page
{
    public UpdateHostelManagerViewModel ViewModel
    {
        get;
    }

    public UpdateHostelManagerPage()
    {
        ViewModel = App.GetService<UpdateHostelManagerViewModel>();
        InitializeComponent();
        var query = "Select Value From Lookup Where Category = 'Gender'";
        Helper.FillComboBox(XamlRoot, query, UpdateManagerGender_ComboBox);
         
    }

    private void SearchManager_ButtonClicked(object sender, RoutedEventArgs e)
    {
        
    }

    private void UpdateManager_ButtonClicked(object sender, RoutedEventArgs e)
    {

    }
    private void FillValues(Users user)
    {
        UpdateManagerID_TextBox.Text = user.Id.ToString();
        UpdateManagerFirstName_TextBox.Text = user.FirstName.ToString();
        UpdateManagerLastName_TextBox.Text = user.LastName.ToString();
        if (user.Username != null)
        {
            UpdateManagerUsername_TextBox.Text = user.Username.ToString();
        }
        if (user.Password != null)
        {
            UpdateManagerPassword_TextBox.Text = user.Password.ToString();
        }
        UpdateManagerContactNumber_TextBox.Text = user.ContactNumber.ToString();
        UpdateManagerEmail_TextBox.Text = user.Email.ToString();
        UpdateManagerAddress_TextBox.Text = user.Address.ToString();
        DateofBirth_DatePicker.SelectedDate = DateTimeOffset.Parse(user.DateofBirth);
        UpdateManagerCity_TextBox.Text = user.City.ToString();
        UpdateManagerGender_ComboBox.SelectedIndex = user.Gender == "Male" ? 0 : 1;
    }
}
