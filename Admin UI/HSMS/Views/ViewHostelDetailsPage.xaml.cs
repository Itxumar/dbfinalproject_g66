﻿using HSMS.ViewModels;

using Microsoft.UI.Xaml.Controls;

namespace HSMS.Views;

public sealed partial class ViewHostelDetailsPage : Page
{
    public ViewHostelDetailsViewModel ViewModel
    {
        get;
    }

    public ViewHostelDetailsPage()
    {
        ViewModel = App.GetService<ViewHostelDetailsViewModel>();
        InitializeComponent();
    }
}
