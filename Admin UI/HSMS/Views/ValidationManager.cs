﻿using Microsoft.UI.Xaml;
using System.Text.RegularExpressions;

namespace HSMS.Views;
internal class ValidationManager
{
    public static bool ValidateFirstName(XamlRoot xamlRoot, string FirstName)
    {

        if (string.IsNullOrEmpty(FirstName))
        {
            Helper.ShowErrorMessage(xamlRoot, "First Name Cannot be Empty");
            return false;
        }
        
        if (FirstName.Length < 3)
        {
            Helper.ShowErrorMessage(xamlRoot, "First Name must be at least 3 characters long");
            return false;
        }

        if (FirstName.Length > 100)
        {
            Helper.ShowErrorMessage(xamlRoot, "First Name must be at most 100 characters long");
            return false;
        }

        var pattern = @"^[A-Z]([A-Za-z\s\.]+)?$";

        if (!Regex.IsMatch(FirstName, pattern))
        {
            Helper.ShowErrorMessage(xamlRoot, "First Name must start with a Capital letter and can only Contain letters, spaces and periods");
            return false;
        }

        return true;
    }

    public static bool ValidateLastName(XamlRoot xamlRoot, string LastName)
    {
        if (LastName.Length < 3)
        {
            Helper.ShowErrorMessage(xamlRoot, "Last Name must be at least 3 characters long");
            return false;
        }

        if (LastName.Length > 100)
        {
            Helper.ShowErrorMessage(xamlRoot, "Last Name must be at most 100 characters long");
            return false;
        }

        var pattern = @"^[A-Z][a-z]+([\s][A-Z][a-z]+)?$";


        if (!Regex.IsMatch(LastName, pattern))
        {
            Helper.ShowErrorMessage(xamlRoot, "Last Name must start with a Capital letter and can only Contain letters, spaces and periods");
            return false;
        }

        return true;
    }

    public static bool ValidateContact(XamlRoot xamlRoot, string Contact)
    {
        if (Contact.Length != 11)
        {
            Helper.ShowErrorMessage(xamlRoot, "Contact must be 11 characters long in the format 03XXXXXXXXX (X Represents Any Digit From 0 to 9)");
            return false;
        }

        var pattern = @"^[0-9]+$";

        if (!Regex.IsMatch(Contact, pattern))
        {
            Helper.ShowErrorMessage(xamlRoot, "Contact must only contain numbers");
            return false;
        }

        pattern = @"^03\d{9}$";

        if (!Regex.IsMatch(Contact, pattern))
        {
            Helper.ShowErrorMessage(xamlRoot, "Contact must start with 03 and can only contain 11 numbers");
            return false;
        }

        return true;
    }

    public static bool ValidateEmail(XamlRoot xamlRoot, string Email)
    {

        if (string.IsNullOrEmpty(Email))
        {
            Helper.ShowErrorMessage(xamlRoot, "Email Cannot be Empty");
            return false;
        }

        if (Email.Length < 5)
        {
            Helper.ShowErrorMessage(xamlRoot, "Email must be at least 5 characters long");
            return false;
        }

        if (Email.Length > 30)
        {
            Helper.ShowErrorMessage(xamlRoot, "Email must be at most 30 characters long");
            return false;
        }

        var pattern = @"^[a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z]{2,}$";

        if (!Regex.IsMatch(Email, pattern))
        {
            Helper.ShowErrorMessage(xamlRoot, "Email must be in the Correct format");
            return false;
        }

        return true;
    }

    public static bool ValidateRegistrationNumber(XamlRoot xamlRoot, string RegistrationNumber)
    {
        if (string.IsNullOrEmpty(RegistrationNumber))
        {
            Helper.ShowErrorMessage(xamlRoot, "Registration Number Cannot be Empty");
            return false;
        }

        if (RegistrationNumber.Length < 8)
        {
            Helper.ShowErrorMessage(xamlRoot, "Registration Number must be at least 8 characters long");
            return false;
        }

        if (RegistrationNumber.Length > 20)
        {
            Helper.ShowErrorMessage(xamlRoot, "Registration Number must be at most 20 characters long");
            return false;
        }

        var pattern = @"^\d{4}-[A-Z]+-\d+$";

        if (!Regex.IsMatch(RegistrationNumber, pattern))
        {
            Helper.ShowErrorMessage(xamlRoot, "Registration Number must be in the Correct format \n [Year in 4 Digits]-[Capital Case Department Code]-[Roll Number]");
            return false;
        }

        return true;
    }

    public static bool ValidateInteger(XamlRoot xamlRoot, string Integer, string IntegerName)
    {

        if (string.IsNullOrEmpty(Integer))
        {
            Helper.ShowErrorMessage(xamlRoot, $"{IntegerName} Cannot be Empty");
            return false;
        }

        var pattern = @"^\d+$";

        if (!Regex.IsMatch(Integer, pattern))
        {
            Helper.ShowErrorMessage(xamlRoot, $"{IntegerName} is not an integer");
            return false;
        }

        return true;
    }

    public static bool ValidateSalary(XamlRoot xamlRoot, string Salary)
    {

        var pattern = @"^\d+$";

        if (!Regex.IsMatch(Salary, pattern))
        {
            Helper.ShowErrorMessage(xamlRoot, "Salary is not an integer");
            return false;
        }

        if (int.Parse(Salary) < 10000)
        {
            Helper.ShowErrorMessage(xamlRoot, "Salary must be at least 10000");
            return false;
        }

        if (int.Parse(Salary) > 1000000)
        {
            Helper.ShowErrorMessage(xamlRoot, "Salary must be at most 1000000");
            return false;
        }

        return true;
    }
}


