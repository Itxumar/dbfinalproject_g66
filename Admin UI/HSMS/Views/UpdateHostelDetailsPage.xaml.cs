﻿using HSMS.ViewModels;
using Microsoft.UI.Xaml;
using Microsoft.UI.Xaml.Controls;

namespace HSMS.Views;

public sealed partial class UpdateHostelDetailsPage : Page
{
    public UpdateHostelDetailsViewModel ViewModel
    {
        get;
    }

    public UpdateHostelDetailsPage()
    {
        ViewModel = App.GetService<UpdateHostelDetailsViewModel>();
        InitializeComponent();
    }

    public void UpdateHostel_ButtonClicked(object sender, RoutedEventArgs e)
    {
    
    }

    public void SearchHostel_ButtonClicked(object sender, RoutedEventArgs e)
    {
    
    }
}
