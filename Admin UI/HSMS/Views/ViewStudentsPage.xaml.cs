﻿using System.Linq;
using HSMS.Models;
using HSMS.ViewModels;
using Microsoft.UI.Xaml;
using Microsoft.UI.Xaml.Controls;
using Microsoft.UI.Xaml.Controls.Primitives;
using Newtonsoft.Json.Linq;

namespace HSMS.Views;

public sealed partial class ViewStudentsPage : Page
{
    public ViewStudentsViewModel ViewModel
    {
        get;
    }

    public ViewStudentsPage()
    {
        ViewModel = App.GetService<ViewStudentsViewModel>();
        InitializeComponent();
        var query = "Select u.Id,u.FirstName,u.LastName,u.Username,u.Password,u.ContactNumber,u.DateOfBirth,u.Email,u.Address,u.City,(Select Value From Lookup Where Id = u.Gender) AS Gender, s.RegistrationNumber, s.EcatMarks, s.FscMarks, s.MatricMarks, (Select value from Lookup where Id = s.DepartmentName) As DepartmentName  From Users u JOIN Student s ON u.Id = s.StudentId";
        Helper.FillColumns(query, dataGrid);

        Helper.FillDataGrid(query, dataGrid,"Student");
    }

    private async void RemoveButton(object sender, RoutedEventArgs e)
    {
        var SelectedIndex = dataGrid.SelectedIndex;

        if (SelectedIndex < 0)
        {
            Helper.ShowErrorMessage(XamlRoot, "Please Select a Row to Remove");
            return;
        }

        Task<int> task = Helper.ShowConfirmationDialog(XamlRoot, "Are You Sure You Want to Remove The Selected Student From the Group?");

        var option = await task;

        if (option == 0)
        {
            return;
        }


        // Get the Selected Row and Convert it to Class Object
        var SelectedItem = (Student)dataGrid.SelectedItem;

        // Get Required Attributes from selected row object
        var UserId = SelectedItem.Id;
        //var StudentId = SelectedItem.StudentId;

        // Perform Operation
        var query1 = $"DELETE FROM [Student] WHERE [StudentId] = {UserId}";
        Helper.ExecuteQuery(XamlRoot, query1);

        var query2 = $"DELETE FROM [Users] Where [Id] = {UserId}";
        Helper.ExecuteQuery(XamlRoot, query2);

        // Show Success Message
        // Helper.ShowMessageBox(XamlRoot, "Success", "Student Removed from Group Successfully");

        // Refresh the DataGrid
        RefreshButton(sender, e);
    }

    private void RefreshButton(object sender, RoutedEventArgs e)
    {
        var query = "SELECT s.StudentId AS [Id], s.RegistrationNumber AS [RegistrationNumber], p.FirstName AS [FirstName], COALESCE(p.LastName, 'NULL') AS [LastName], COALESCE(p.ContactNumber, 'NULL') AS [ContactNumber], p.Email, COALESCE(CONVERT(varchar(10), p.DateOfBirth, 105), 'NULL') AS [DateOfBirth], COALESCE(l.Value, 'NULL') AS [Gender] FROM Student s JOIN Users p ON s.StudentId = p.Id LEFT JOIN Lookup l ON p.Gender = l.Id AND l.Category = 'Gender'";
        Helper.FillColumns(query, dataGrid);

        Helper.FillDataGrid(query, dataGrid, "Student");

    }

    // In View Grid Page --  Edit Button Click Event
    private void EditButton(object sender, RoutedEventArgs e)
    {
        var selectedIndex = dataGrid.SelectedIndex;

        if (selectedIndex == -1)
        {
            Helper.ShowErrorMessage(XamlRoot, "No Row is Selected! Please Select a Row to Edit it.");
            return;
        }


        // Get Selected Row as Class Object
        var selectedItem = dataGrid.SelectedItem as Student;


        // Navigate to Update Page and Pass the Row Object to Update Page
        Frame.Navigate(typeof(UpdateStudentPage), selectedItem);

    }

}
