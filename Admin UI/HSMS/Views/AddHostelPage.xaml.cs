﻿using HSMS.Models;
using System.Data.SqlClient;
using HSMS.ViewModels;
using Microsoft.UI.Xaml;
using Microsoft.UI.Xaml.Controls;
using Windows.Security.Cryptography.Core;
using WinUIEx;

namespace HSMS.Views;

public sealed partial class AddHostelPage : Page
{
    public AddHostelViewModel ViewModel
    {
        get;
    }

    public AddHostelPage()
    {
        ViewModel = App.GetService<AddHostelViewModel>();
        InitializeComponent();

        var query1 = "Select Value From Lookup Where Category = 'HOSTEL_NAME'";
        var query = "Select Value From Lookup Where Category = 'HOSTEL_TYPE'";

        Helper.FillComboBox(XamlRoot, query1,HostelName_ComboBox);

        Helper.FillComboBox(XamlRoot, query, HostelType_ComboBox);


    }
    public void AddHostel_ButtonClicked(object sender, RoutedEventArgs e)
    {
        var hostelname = HostelName_ComboBox.SelectedIndex + 17 ;
        var hosteltype = HostelType_ComboBox.SelectedIndex +15;

        var con = Configuration.getInstance().getConnection();
        SqlCommand cmd1 = new SqlCommand("Insert Into Hostel(HostelName,HostelType) values (@HostelName,@HostelType)", con);
        cmd1.Parameters.AddWithValue("@HostelName", hostelname);
        cmd1.Parameters.AddWithValue("@HostelType", hosteltype);
        cmd1.ExecuteNonQuery();



    }
}
