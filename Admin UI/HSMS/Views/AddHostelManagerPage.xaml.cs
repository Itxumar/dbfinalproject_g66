﻿using HSMS.ViewModels;
using Microsoft.UI.Xaml;
using Microsoft.UI.Xaml.Controls;
using System.Data.SqlClient;
using System.Diagnostics;

namespace HSMS.Views;

public sealed partial class AddHostelManagerPage : Page
{
    public AddHostelManagerViewModel ViewModel
    {
        get;
    }

    public AddHostelManagerPage()
    {
        ViewModel = App.GetService<AddHostelManagerViewModel>();
        InitializeComponent();

        var query = "Select Value From Lookup Where Category = 'Gender'";
        var query1 = "Select HostelID From Hostel";
        Helper.FillComboBox(XamlRoot, query, ManagerGender_ComboBox);
        Helper.FillComboBox(XamlRoot, query1, HostelId_ComboBox);
    }

    private void AddManager_ButtonClicked(Object sender, RoutedEventArgs e)
    {
        var firstname = ManagerFirstName_TextBox.Text;
        var lastname = ManagerLastName_TextBox.Text;
        var email = ManagerEmail_TextBox.Text;
        var username = ManagerUsername_TextBox.Text;
        var password = ManagerPassword_TextBox.Text;
        var contactnumber = ManagerContactNumber_TextBox.Text;
        var address = ManagerAddress_TextBox.Text;
        var city = ManagerCity_TextBox.Text;
        var Dateofbirth = ManagerDateofBirth_DatePicker.Date;
        var gender = ManagerGender_ComboBox.SelectedIndex + 1;
        var hostelid = HostelId_ComboBox.SelectedIndex;
        


        var con = Configuration.getInstance().getConnection();
        SqlCommand cmd = new SqlCommand("Insert Into Users (Username,Password,FirstName,LastName,ContactNumber,Email,Address,DateOfBirth,City,Gender) values (@Username,@Password,@FirstName,@LastName,@ContactNumber,@Email,@Address,@DateOfBirth,@City,@Gender)", con);
        cmd.Parameters.AddWithValue("@Username", username);
        cmd.Parameters.AddWithValue("@Password", password);
        cmd.Parameters.AddWithValue("@FirstName", firstname);
        cmd.Parameters.AddWithValue("@LastName", lastname);
        cmd.Parameters.AddWithValue("@ContactNumber", contactnumber);
        cmd.Parameters.AddWithValue("@Email", email);
        cmd.Parameters.AddWithValue("@Address", address);
        cmd.Parameters.AddWithValue("@DateOfBirth", Dateofbirth);
        cmd.Parameters.AddWithValue("@City", city);
        cmd.Parameters.AddWithValue("@Gender", gender);

        cmd.ExecuteNonQuery();

        SqlCommand cmd1 = new SqlCommand("Insert Into HostelManager(HostelId) values (@HostelId)", con);
        cmd1.Parameters.AddWithValue("@HostelId", hostelid);
        cmd1.ExecuteNonQuery();
        Helper.ShowMessageBox(XamlRoot,"Success","Successfully saved");

    }
}
