﻿
using System.Data.SqlClient;


#pragma warning disable CS8618
#pragma warning disable IDE0161

namespace HSMS.Views
{
    class Configuration
    {
        private readonly string ConnectionStr = @"Data Source=(local);Initial Catalog=HostelManagementSystemDB;Integrated Security=True";
        private readonly SqlConnection con;
        private static Configuration _instance;
        public static Configuration getInstance()
        {
            _instance ??= new Configuration();
            return _instance;
        }
        private Configuration()
        {
            con = new SqlConnection(ConnectionStr);
            con.Open();
        }
        public SqlConnection getConnection()
        {
            return con;
        }
    }
}






